
package http;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Cliente {
	private int codigo;
	private String nome, senha, descricao;
	private int saldo;

	public Cliente(int codigo, String nome, String senha, String descricao, Integer saldo) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.senha = senha;
		this.descricao = descricao;
		this.saldo = saldo;
	}

	public Cliente() {
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getSaldo() {
		return saldo;
	}

	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}
}
