package repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entity.ClienteEntity;

public class ClienteRepository {

	private final EntityManagerFactory entityManagerFactory;

	private final EntityManager entityManager;

	public ClienteRepository() {
		/*
		 * CRIANDO O NOSSO EntityManagerFactory COM AS PORPRIEDADOS DO ARQUIVO
		 * persistence.xml
		 */
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistence_db_usuario");
		this.entityManager = this.entityManagerFactory.createEntityManager();
	}

	/**
	 * SACA DA CONTA DO CLIENTE
	 */
	public void Sacar(Integer saldo) {
		this.entityManager.getTransaction().begin();
//		this.entityManager.persist(clienteEntity);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * CRIA UM NOVO REGISTRO NO BANCO DE DADOS
	 */
	public void Salvar(ClienteEntity clienteEntity) {
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(clienteEntity);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * ALTERA UM REGISTRO CADASTRADO
	 */
	public void LancarDeposito(ClienteEntity clienteEntity) {
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(clienteEntity);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * ALTERA UM REGISTRO CADASTRADO
	 */
	public void LancarSaque(ClienteEntity clienteEntity) {
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(clienteEntity);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * RETORNA TODOS OS CLIENTES CADASTRADAS NO BANCO DE DADOS
	 */
	@SuppressWarnings("unchecked")
	public List<ClienteEntity> TodosClientes() {
		return this.entityManager.createQuery("SELECT p FROM ClienteEntity p ORDER BY p.codigo").getResultList();
	}

//	/**
//	 * CONSULTA UMA PESSOA CADASTRA PELO CÓDIGO
//	 */
//	public ClienteEntity GetSaldo(Integer saldo) {
//		return this.entityManager.find(ClienteEntity.class, saldo);
//	}

	/**
	 * CONSULTA UMA PESSOA CADASTRA PELO CÓDIGO
	 */
	public ClienteEntity GetCliente(Integer codigo) {
		return this.entityManager.find(ClienteEntity.class, codigo);
	}

	/**
	 * EXCLUINDO UM REGISTRO PELO CÓDIGO
	 **/
	public void DeletarCliente(Integer codigo) {
		ClienteEntity cliente = this.GetCliente(codigo);

		this.entityManager.getTransaction().begin();
		this.entityManager.remove(cliente);
		this.entityManager.getTransaction().commit();
	}

}
