package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.omg.CORBA.portable.ApplicationException;

import entity.ClienteEntity;
import http.Cliente;
import repository.ClienteRepository;

//	Cliente pode:
//		sacar, depositar e ver saldo;








@Path("/service")
public class ServiceController {

	private final ClienteRepository clienteRepository = new ClienteRepository();

	/* GERENTE */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/todosClientes")
	public List<Cliente> TodosClientes() throws IOException, ApplicationException {

		List<Cliente> clientes = new ArrayList<Cliente>();

		List<ClienteEntity> listaEnClienteEntities = clienteRepository.TodosClientes();

		for (ClienteEntity clienteEntity : listaEnClienteEntities) {
			clientes.add(new Cliente(clienteEntity.getCodigo(), 
					clienteEntity.getDescricao(), clienteEntity.getNome(),
					clienteEntity.getSenha(), clienteEntity.getSaldo()));
		}
		return clientes;
	}                
	
	
	
	
	
	
	

	@DELETE
	@Produces("application/json; charset=UTF-8")
	@Path("/deletarCliente/{codigo}")
	public String DeletarCliente(@javax.ws.rs.PathParam("codigo") Integer codigo) {

		try {
			clienteRepository.DeletarCliente(codigo);
			return "Registro deletado com sucesso!";
		} catch (Exception e) {
			return "Erro ao excluir o cliente! " + e.getMessage();
		}
	}

	@POST
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/addCliente")
	public String AddCliente(Cliente cliente) {
		// TODO implementation
		ClienteEntity clienteEntity = new ClienteEntity();

		try {
			clienteEntity.setNome(cliente.getNome());
			clienteEntity.setDescricao(cliente.getDescricao());
			clienteEntity.setSenha(cliente.getSenha());
			clienteEntity.setSaldo(0);

			clienteRepository.Salvar(clienteEntity);

			return "Cliente cadastrado com sucesso!";
		} catch (Exception e) {
			return "Cliente NÃO CADASTRADO!";
			// TODO: handle exception
		}
	}

	/*
	 * CLIENTE
	 */
	@PUT
	@Produces("application/json; charset=UTF-8")
	@Consumes("application/json; charset=UTF-8")
	@Path("/lancarDeposito")
	public String LancarDeposito(Cliente cliente, int valorDeposito) {
		ClienteEntity clienteEntity = new ClienteEntity();

		try {
			clienteEntity.setSaldo(cliente.getSaldo());
			clienteRepository.LancarDeposito(clienteEntity);
			return "Depósito lançado com sucesso!";
		} catch (Exception e) {
			// TODO: handle exception
			return "Depósito NÃO lançado!" + e.getMessage();
		}
	}

	@PUT
	@Produces("application/json; charset=UTF-8")
	@Consumes("application/json; charset=UTF-8")
	@Path("/lancarSaque")
	public String LancarSaque(Cliente cliente, int valorSaque) {
		ClienteEntity clienteEntity = new ClienteEntity();
		try {
			if (valorSaque != 0) {
				clienteEntity.getSaldo();
				int novoSaldo = clienteEntity.setSaldo(cliente.getSaldo()) - valorSaque;
				clienteEntity.setSaldo(novoSaldo);
				clienteRepository.LancarSaque(clienteEntity);
			}
			return "Saque realizado com sucesso!";
		} catch (Exception e) {
			// TODO: handle exception
			return "Saque NÃO realizado!";
		}
	}

	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/verSaldo/{codigo}")
	public String VerSaldo(@javax.ws.rs.PathParam("codigo") Integer codigo, Cliente cliente) {
		// TODO implementar e testar os métodos do cliente
		try {
			clienteRepository.GetCliente(codigo);

			return "Saldo";
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}
	}

//	@SuppressWarnings("unused")
//	@GET
//	@Produces("application/json; charset=UTF-8")
//	@Path("/VerSaldo/{codigo}")
//	public Cliente VerSaldo(@PathParam("codigo") Integer codigo) {
//		ClienteEntity entity = clienteRepository.GetPessoa(codigo);
//
//		if (entity != null) {
//			return new Cliente(entity.getSaldo());
//		}
//		return null;
//	}

//	@POST
//	@Produces("application/json; charset=UTF-8")
//	@Consumes("application/json; charset=UTF-8")
//	@Path("/sacar")
//	public String Sacar(Cliente cliente, int valorSaque) {
//
//		ClienteRepository clienteRepository = new ClienteRepository();
//		try {
//			if (cliente.getSaldo() <= 0) {
//				// print "saldo insuficiente"
//				return "Saldo insuficiente";
//			} else {
//				clienteRepository.Sacar(cliente.setSaldo(cliente.getSaldo() - valorSaque));
//				return "Saque efetuado";
//			}
//		} catch (Exception e) {
//			return "Erro em SACAR " + e.getMessage();
//		}
//	}
//
//	@POST
//	@Produces("application/json; charset=UTF-8")
//	@Consumes("application/json; charset=UTF-8")
//	@Path("/depositar")
//	public String Depositar(Cliente cliente, Integer valorDep) {
//		ClienteRepository clienteRepository = new ClienteRepository();
//
//		try {
//
//			clienteRepository.Sacar(cliente.setSaldo(cliente.getSaldo() + valorDep));
//			return "Depósito reaizado.";
//
//		} catch (Exception e) {
//			return "Erro ao DEPOSITAR " + e.getMessage();
//		}
//	}

}
